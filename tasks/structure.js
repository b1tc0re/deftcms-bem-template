let path = require('path'),
    fs = require('fs'),
    mkdirp = require('mkdirp');

module.exports = {

    /**
     * Создать структуру папок
     * @param {object} options
     */
    createStructure: function (options) {

        if( fs.existsSync(options.templatePath) ) {
            this._fnDeleteFolderRecursive(options.bundles.templatePath);
        }

        mkdirp(path.join(options.bundles.templatePath, 'template/modules'));
        mkdirp(path.join(options.bundles.templatePath, 'assets/js'));
        mkdirp(path.join(options.bundles.templatePath, 'assets/css'));
    },

    /**
     * Создать структуру для модуля
     * @param {object} options
     * @param {object} config
     * @return {string}
     */
    createStructureModule: function(options, config) {
        let targetDir = path.join(options.bundles.templatePath, 'template', config.module ? 'modules': '');

        targetDir = path.join(targetDir, config.module || '');
        if( !fs.existsSync(path) ) {
            mkdirp( targetDir );
        }

        return  targetDir;
    },

    /**
     * Метод удаляет рекурсивно директории и содержимое из директорий
     * @param {String} _path Путь к директории
     * @private
     */
    _fnDeleteFolderRecursive : function(_path) {

        if ( false === fs.existsSync(_path) ) {
            return;
        }

        fs.readdirSync(_path).forEach(function(file) {
            let curPath = path.join(_path, file);

            if (fs.lstatSync(curPath).isDirectory()) {
                this._fnDeleteFolderRecursive(curPath);
            } else {
                fs.unlinkSync(curPath);
            }
        }.bind(this));

        fs.rmdirSync(_path);
    },
};