module.exports = {

    /**
     * Обработка шаблона
     * @param {string} rawHtml
     * @return {string}
     */
    start: function (rawHtml) {
        rawHtml = this._normalizeBemQoute(rawHtml);
        rawHtml = this._replaceCommand(rawHtml);
        rawHtml = this._replaceLinks(rawHtml);
        rawHtml = this._replaceBemVarTypes(rawHtml);
        rawHtml = rawHtml.replace(/&quot;/gm, '"');
        return rawHtml;
    },

    /**
     * Нормализовать qoute
     * @param {string} content
     * @return {string}
     * @private
     */
    _normalizeBemQoute : function(content) {
        const regex = /data-bem="(.*?)"/gm;
        return content.replace(regex, function (strs, content) {
            content = content.replace(/&quot;/gm, '"');
            return strs.replace(strs, "data-bem='" + content + "'");
        });
    },

    /**
     * Замена данных
     * @param {string} content
     * @return {string}
     * @private
     */
    _replaceCommand: function (content) {
        let regex = /<!--replaceStart=["'(&quot;)]+(.*?)["'(&quot;)]-->(.*?)<!--replaceEnd-->/gms;

        return content.replace(regex, function (_, match) {
            return match.replace(/}}&quot/gm, '}}').replace(/&quot;/gm, '"');
        });
    },

    /**
     * Обработка ссылок
     * @param {string} content
     * @return {string}
     * @private
     */
    _replaceLinks : function (content) {
        let regex = /<a\s+(?:[^>]*?\s+)?href=(["'])(.*?)(["'])/gsm;

        return content.replace(regex, (_, agrs, match) => {
            let link = match.replace(/.*[#!](.*?)$/g, '$1');
            return _.replace(match, this.normalizeUrl(link) );
        });
    },

    /**
     * Примести адреса к нормальному виду
     * @param {string} url
     * @return {string}
     */
    normalizeUrl: function(url) {

        if(  url.indexOf("{{") >= 0 && url.indexOf("}}") >= 0 ) {
            return this._fnReduceDoubleSlash(url);
        }

        if( url.startsWith('https://') ) {
            return this._fnTrim(url, '/') + '/';
        }

        url = '/' + this._fnTrim(url, '/') + '/';

        return this._fnReduceDoubleSlash(url);
    },

    /**
     * Заменить значение в data-bem
     * @param {string} content
     * @return {string}
     * @private
     */
    _replaceBemVarTypes : function (content) {
        const regex = /["'(&quot;)]+(bool:|int:|object:)+(.*?)}}["'(&quot;)]/gs;
        return content.replace(regex,"$2 }} ");
    },

    /// region Вспомогательные функции

    /**
     * Удалить двойной слэш
     * @param {string} content
     * @return {string}
     * @private
     */
    _fnReduceDoubleSlash: function (content) {
        let regex = /(^|[^:])\/\/+/g;
        return content.replace(regex, '$1/');
    },

    /**
     * Удаляет пробелы (или другие символы) из начала и конца строки
     * @param {string} str
     * @param {string} chr
     * @return {string}
     * @private
     */
    _fnTrim : function (str, chr) {
        var rgxtrim = (!chr) ? new RegExp('^\\s+|\\s+$', 'g') : new RegExp('^'+chr+'+|'+chr+'+$', 'g');
        return str.replace(rgxtrim, '');
    },

    /**
     * Удаляет пробелы (или другие символы) из конца строки
     * @param {string} str
     * @param {string} chr
     * @return {string}
     * @private
     */
    _fnRtrim : function (str, chr) {
        var rgxtrim = (!chr) ? new RegExp('\\s+$') : new RegExp(chr+'+$');
        return str.replace(rgxtrim, '');
    },

    /**
     * Удаляет пробелы (или другие символы) из начала строки
     * @param {string} str
     * @param {string} chr
     * @return {string}
     * @private
     */
    _fnLtrim : function (str, chr) {
        var rgxtrim = (!chr) ? new RegExp('^\\s+') : new RegExp('^'+chr+'+');
        return str.replace(rgxtrim, '');
    }

    ///endregion
};
