let path = require('path'),
    fs = require('fs'),
    imagemin = require('imagemin'),
    imageminMozjpeg = require('imagemin-mozjpeg'),
    imageminPngquant = require('imagemin-pngquant'),
    imageminSvgo = require('imagemin-svgo'),
    imageminWebp = require('imagemin-webp'),
    minify = require('html-minifier').minify,
    { lstatSync, readdirSync } = require('fs'),
    { join } = require('path');

/**
 * Imagemin Options
 *
 * @see https://github.com/imagemin/imagemin
 */
const options = {
    /**
     * JPEG compression plugin options
     *
     * @see https://github.com/imagemin/imagemin-mozjpeg
     */
    mozjpegOptions: {
        progressive: true,
        quality: 80
    },
    /**
     * PNG compression plugin options
     *
     * @see https://github.com/imagemin/imagemin-pngquant
     */
    pngquantOptions: {
        quality: [0.3, 0.7]
    },
    /**
     * SVG compression plugin
     *
     * @see https://github.com/imagemin/imagemin-svgo
     */
    svgoOptions: {
        removeDoctype: true,
        removeXMLProcInst: true,
        removeComments: true,
        removeMetadata: true,
        removeEditorsNSData: true,
        cleanupAttrs: true,
        inlineStyles: true,
        minifyStyles: true,
        convertStyleToAttrs: true,
        cleanupIDs: true,
        removeUselessDefs: true,
        convertColors: true,
        removeViewBox: true,
        convertTransform: true,
        removeEmptyAttrs: true,
        removeEmptyContainers: true,
        mergePaths: true,
        removeUnusedNS: true,
        removeTitle: true,
        removeDesc: true,
    },

    imageminWebp: {
        quality: 80
    },
};

/**
 * Helper functions to get directories / sub-directories
 *
 * @see https://stackoverflow.com/a/40896897/4364074
 */
const isDirectory = source => lstatSync(source).isDirectory();
const getDirectories = source =>
    readdirSync(source)
        .map(name => join(source, name))
        .filter(isDirectory);
const getDirectoriesRecursive = source => [
    source,
    ...getDirectories(source)
        .map(getDirectoriesRecursive)
        .reduce((a, b) => a.concat(b), [])
];

module.exports = {

    /**
     * Подсчет байтов
     * @param str
     * @return {*}
     */
    lengthInUtf8Bytes: function(str) {
        // Matches only the 10.. bytes that are non-initial characters in a multi-byte sequence.
        var m = encodeURIComponent(str).match(/%[89ABab]/g);
        return str.length + (m ? m.length : 0);
    },

    /**
     * Оптимизация html кода
     * @param {string} rawHtml сырой html
     * @param {object} options Настройки проекта
     * @return {string} оптимиированные html
     */
    optimizeHTML: function (rawHtml, options) {
        if( !options.optimize || !options.html ) {
            return rawHtml;
        }
        console.log('Start optimize html code. Input size :', this.lengthInUtf8Bytes(rawHtml));
        rawHtml = minify(rawHtml, options.html || {});
        console.log('End optimize html code. Output size :', this.lengthInUtf8Bytes(rawHtml));

        return rawHtml;
    },

    /**
     * Оптимизация CSS,JS и изоброжений
     * @param {object} options  Настройки проекта
     * @return {exports}
     */
    optimizeImages: function(options) {
        options = options.optimize || {};
        options.image && this._optimizeImages(options.image);
    },

    /**
     * Оптимизация изоброжений
     * @param _options
     * @private
     */
    _optimizeImages: function (_options) {

        Array.isArray(_options.input) || (_options.input = [ _options.input ]);

        try {
            console.log('Beginning image compression...');

            let imageDirs = [];
            _options.input.map(dirname =>(imageDirs = imageDirs.concat(getDirectoriesRecursive(dirname))));

            imageDirs.forEach((dir) => {
                let input = [`${dir}/*.{jpg,png,svg}`],
                    output = path.join(_options.output, dir);
                this.optimizeImagesAsync(input, output);
            });

            console.log('Finished compressing all images!');

        } catch (e) { }
    },

    /**
     *
     * @param input
     * @param output
     * @return {Promise<never>}
     */
    optimizeImagesAsync: async function (input, output) {
        return await imagemin(input, output, {
            plugins: [
                imageminMozjpeg(options['mozjpegOptions']),
                imageminPngquant(options['pngquantOptions']),
                imageminSvgo(options['svgoOptions']),
                imageminWebp(options['imageminWebp'])
            ]
        });
    }
};
