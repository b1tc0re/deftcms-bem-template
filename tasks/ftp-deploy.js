let deploy = require("ftp-deploy"),
    path = require("path"),
    fs = require("fs");

module.exports = {

    /**
     * Project root directory
     */
    rootDir: process.cwd(),

    /**
     * Project options
     */
    options: {},

    /**
     *
     * @param {deployTasks} options FTP Deploy options
     */
    deploy: function (deployTasks) {

        if( Object.keys(deployTasks).length === 0 ) {
            console.error("Deploy options is empty");
            return false;
        }


        deployTasks.forEach((task) => {
            console.log("Start deploy task " + task.localRoot);
            let ftpDeploy = new deploy();
            ftpDeploy.deploy(task)
                .then(res => console.log('finished:', res))
                .catch(err => console.error("FTP deploy:", err));
        });
    }
};
