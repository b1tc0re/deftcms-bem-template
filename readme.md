# Этот код используетя для перевода bem шаблонов в twig

### Инициализация
Для использования парсера нужно его инициализировать.
```js
let template = require("bem-htmltwig"),
    options = {};

template.initialize(options).build();
```

### Декларация модуля
В шаблонах нужно описывать модуль блоком template-config смотреть ниже пример.
```js
module.exports = {
    block: 'page',
    title: 'Title of the page',
    favicon: '/favicon.ico',
    head: [
        { elem: 'meta', attrs: { name: 'description', content: '' } },
        { elem: 'meta', attrs: { name: 'viewport', content: 'width=device-width, initial-scale=1' } },
        { elem: 'css', url: 'index.min.css' }
    ],
    scripts: [{ elem: 'js', url: 'index.min.js' }],
    mods: { theme: 'islands' },
    content: [
        {
            block : 'template-config',
            js : { module : 'favorite', id : '#wrapper', name : 'index.tpl' }
        },
        {
            block : 'content',
            attrs : { id : 'wrapper' },
            content : 'HTML '
        }
    ]
};
```