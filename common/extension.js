Array.prototype.inArray = function(val) {
    for(let i = 0, l = this.length; i < l; i++)	{
        if(this[i] === val) {
            return true;
        }
    }
    return false;
};