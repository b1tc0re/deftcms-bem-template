let path = require('path'),
    fs = require('fs'),
    structure = require('./tasks/structure'),
    parse = require('./tasks/parse'),
    deploy = require('./tasks/ftp-deploy'),
    optimize = require('./tasks/optimize'),
    extension = require('./common/extension'),
    rs = require("randomstring"),
    beautify = require('js-beautify'),
    { JSDOM } = require("jsdom");

module.exports = {

    /**
     * Уникальный ID для сброса кэша
     */
    uniqId : rs.generate(),

    /**
     * Настройки
     */
    options : {
        bundles: {
            "bundlesPath": "bundles",
            "templatePath": "theme",
            "deployBundlesName": "desktop.bundles",
            "excludeBundle": ["merged", "stub"]
        },
        "hasProduction": false,
        "theme": "default",
        "rootDir": process.cwd(),
        "convertType": 'merged',
        "moduleName" : null,
        jqueryUrl: "//yastatic.net/jquery/3.3.1/jquery.min.js"
    },

    /**
     * Инициализация настроек
     *
     * @param {object} _options
     * @return {exports}
     */
    initialize : function (_options) {
        let optionsPath = path.join(this.options.rootDir, '.deftcms.json');

        if( !fs.existsSync(optionsPath) && !_options ) {
            new Error('Отсуствует файл конфигурации. ' + optionsPath);
        }

        // Присвоить настройки из конфигурационного файла
        if( fs.existsSync(optionsPath) ) {
            let options = require(optionsPath);
            this.options = Object.assign(this.options, options);
        }

        // Присвоить переданные настройки
        this.options = Object.assign(this.options, _options || {});

        if( process.env.NODE_ENV === 'production' && this.options.hasProduction === false ) {
            this.options.hasProduction = true;
        }

        // Назначить пути от корня проекта
        this.options.bundles.bundlesPath = path.join(process.cwd(), this.options.bundles.bundlesPath);
        this.options.bundles.templatePath = path.join(process.cwd(), this.options.bundles.templatePath, this.options.theme);

        return this;
    },

    /**
     * Запуск преоброзованияы
     */
    build: function() {
        structure.createStructure(this.options);
        let bundleDir = path.join(this.options.bundles.bundlesPath, this.options.bundles.deployBundlesName);

        this.options.convertType === 'merged' ? this.copyAssets(bundleDir) : this.copyModule(bundleDir);

        fs.readdirSync(bundleDir).forEach(fileName => {
            if( this.options.bundles.excludeBundle.inArray(fileName) === false ) {
                let targetHtmlFile = path.join(bundleDir, fileName, fileName + '.html');
                this.documentParse(targetHtmlFile, fileName, fileName === 'main');
            }
        });

        // Оптимизация Изоброжений, стилей и скриптов
        if( this.options.hasProduction === true ) {
            optimize.optimizeImages(this.options);
            deploy.deploy(this.options["deploy-tasks"] || {});
        }
    },

    /**
     * Метод копирует merged файлы
     * @param targetDir
     */
    copyAssets: function (targetDir) {

        let source = path.join(targetDir, 'merged');
        let target = path.join(this.options.bundles.templatePath, 'assets');

        fs.createReadStream(path.join(source, 'merged.min.css'))
            .pipe(fs.createWriteStream(path.join(target, 'css/common.min.css')));

        fs.createReadStream(path.join(source, 'merged.min.js'))
            .pipe(fs.createWriteStream(path.join(target, 'js/common.min.js')));
    },

    /**
     * Скопировать файлы для модуля
     * @param targetDir
     */
    copyModule: function(targetDir) {
        let name = this.options.moduleName ? this.options.moduleName : 'common';

        let source = path.join(targetDir, 'project');
        let target = path.join(this.options.bundles.templatePath, 'assets');

        fs.createReadStream(path.join(source, 'project.min.css'))
            .pipe(fs.createWriteStream(path.join(target, 'css/'+name+'.min.css')));

        fs.createReadStream(path.join(source, 'project.min.js'))
            .pipe(fs.createWriteStream(path.join(target, 'js/'+name+'.min.js')));
    },

    /**
     * Обработка HTML шаблона
     * @param {string} targetHtmlFile Путь к файлу с сырым HTML
     * @param {string} file Имя файла
     * @param {boolean} hasMain Является ли шаблон шлавным
     * @returns {boolean}
     */
    documentParse: function (targetHtmlFile, file, hasMain) {
        let rawHtml = fs.readFileSync(targetHtmlFile, 'utf8');
        let dom = new JSDOM(rawHtml, { includeNodeLocations: true } );

        let document        = dom.window.document;
        let body            = document.body;
        let templateOptions = body.querySelector('.template-config');

        if( templateOptions === null ) {
            console.error('В шаблоне '+file+' не описан модуль.');
            return false;
        }

        templateOptions =  JSON.parse(templateOptions.getAttribute('data-bem'))['template-config'];
        templateOptions.module === false && (hasMain = true);

        body.querySelector('.template-config').outerHTML = "";

        document.querySelector('head').prepend("\n{{ headers }}");
        document.querySelector('head').append("\n{{ stylesheet }}");
        document.querySelector('head').append("\n{% include 'favicon.tpl' %}");

        body.append("\n{{ javascript }}");

        let htmlSelected = document.querySelector(templateOptions.id);
        htmlSelected && htmlSelected.removeAttribute('id');

        if( hasMain === true ) {
            htmlSelected = document.documentElement;
            document.getElementsByTagName('title')[0].remove();
        }

        if( htmlSelected === null ) {
            console.error('Для шаблона '+file+' не найден основной контент по ID ' + templateOptions.id);
            return false;
        }

        let outerHTML = hasMain === true ? htmlSelected.outerHTML : htmlSelected.innerHTML;

        if( hasMain === true ) {
            outerHTML = this.prepareMainPage(outerHTML, file);
        }

        return this.buildTemplate(outerHTML, templateOptions);
    },

    /**
     * Обработка шаблона главной страницы
     * @param {string} outerHtml HTML
     * @returns {string}
     */
    prepareMainPage: function (outerHtml, name) {
        outerHtml = "<!DOCTYPE html>\n" + outerHtml;

        // Изменить название подключаемы файлов
        outerHtml = outerHtml.replace(name + '.min.css', "{{ css_path }}common.min.css?q=" + this.uniqId );
        outerHtml = outerHtml.replace(name + '.min.js', "{{ js_path }}common.min.js?q=" + this.uniqId );

        // Удалить лишние тэги
        outerHtml = outerHtml.replace('<meta charset="utf-8">', "");
        outerHtml = outerHtml.replace(/<script.?src=['"](.*?)['"]><\/script>/, "<script src='"+this.options.jqueryUrl+"'></script>");

        return outerHtml;
    },

    /**
     * Обработать шаблон и сохранить
     * @param {string} rawHtml сырой Html
     * @param {object} options настройки шаблона
     */
    buildTemplate: function (rawHtml, options) {
        let destPath = structure.createStructureModule(this.options, options),
            targetPath = path.join(destPath, options.name );

        rawHtml = beautify.html(rawHtml);
        rawHtml = parse.start(rawHtml);

        if( this.options.hasProduction === true ) {
            rawHtml = optimize.optimizeHTML(rawHtml, this.options);
        }

        this.ensureDirectoryExistence(targetPath);

        // Записать HTML в файл
        fs.writeFileSync(targetPath, rawHtml);
    },

    /**
     * Создать директорию для файла
     * @param filePath
     * @return {boolean}
     */
    ensureDirectoryExistence : function (filePath) {
        var dirname = path.dirname(filePath);
        if (fs.existsSync(dirname)) {
            return true;
        }
        this.ensureDirectoryExistence(dirname);
        fs.mkdirSync(dirname);
    }
};